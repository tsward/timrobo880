#!/usr/bin/env python

import rospy
import math
import numpy as np
import tf
from nav_msgs.msg import Odometry
from kobuki_msgs.msg import BumperEvent
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from collections import deque
from numpy import linalg as LA

# for move_base
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
import time




# TURTLEBOT SPECS

MAX_LIN_VEL = .7
MAX_ROT_VEL = 3.14

BASE_DIAMETER = .23
WHEEL_RADIUS = .035
WHEEL_WIDTH = .021
TICKS_PER_REVOLUTION = 52
PULSES_PER_REVOLUTION = 13
TICK_TO_METER = .000085292090497737556558
METER_TO_TICK = 11724.41658029856624751591
MILLIMITER_TO_TICK = 11.72441658029856624751591
TICK_TO_RADIAN = .002436916871363930187454

# NOTE: this still needs tweeking (sharp) angles)
COLLISION_AVOIDANCE_VAL = 0.6
#COLLISION_AVOIDANCE_VAL = 1

KINECT_ANGLE_MIN = -0.521567881107
KINECT_ANGLE_MAX = 0.524276316166
KINECT_ANGLE_INC = 0.00163668883033


# from TA's bug2Wrapper.py goal distance acceptance
ACCEPTABLE_GOAL_DIST = 0.4
  
# This value is from nothing more than trial and error (and a noisy sensor) 
SENSOR_TOLERANCE = 0.02

# keep the robot aligned to the m-line during goal seek (it will veer off)
M_LINE_THRESHOLD = 0.05



class STATUS:
	GOALSEEK = 1
	WALLFOLLOW = 2
	FACE_GOAL = 3
	ALIGN = 4
	
	
class WF_STATUS:
	rotating = 1
	driving_straight = 2
	facing_obstacle = 3
	NOT_SET = 4
	
	
class Bug2:

       
	#--Call back functions-------------------------------------
    def bumper_callback(self, bumper_msg):
        """ callback to handle bumper messages"""
        print("BUMPER PRESSED: stopping turtlebot " + str(bumper_msg))
        # in this case STOP TURTLEBOT (algorithm failed)
        #self.shutdown()
        self.bumper_pressed = True
        
    def odom_callback(self, odom_msg):
        """ callback to handle odometry messages"""
        self.x = odom_msg.pose.pose.position.x
        self.y = odom_msg.pose.pose.position.y
        self.odom = self.copy_odom(odom_msg)
        return

    def laser_scan_callback(self, laser_msg):
        self.obs_ranges = laser_msg.ranges

    def copy_odom(self, message=-1):
        """
            copy an odometry message (deepcopy did not work for me
            if you figure it out let me know :P).
            If no arguments are given it will the message the solution
            uses
        """
        if message == -1:
            message = self.odom
        ret = Odometry()
        ret.pose.pose.position.x = message.pose.pose.position.x
        ret.pose.pose.position.y = message.pose.pose.position.y
        ret.pose.pose.position.z = message.pose.pose.position.z
        ret.pose.pose.orientation.x = message.pose.pose.orientation.x
        ret.pose.pose.orientation.x = message.pose.pose.orientation.x
        ret.pose.pose.orientation.y = message.pose.pose.orientation.y
        ret.pose.pose.orientation.z = message.pose.pose.orientation.z
        ret.pose.pose.orientation.w = message.pose.pose.orientation.w
        ret.twist.twist.linear.x = message.twist.twist.linear.x
        ret.twist.twist.linear.y = message.twist.twist.linear.y
        ret.twist.twist.linear.z = message.twist.twist.linear.z
        ret.twist.twist.angular.x = message.twist.twist.angular.x
        ret.twist.twist.angular.y = message.twist.twist.angular.y
        ret.twist.twist.angular.z = message.twist.twist.angular.z
        return ret
    
    def vel_from_wheels(self, u1, u2, time):
        """
        Compute translational and rotational velocities from the velocities of the left and right wheel.
        """
        self.twist_msg = Twist()
        linear_velocity = np.mean((u1, u2))  # by definition
        angular_velocity = (u2 - u1) / BASE_DIAMETER # by definition
        #Compose the twist message
        self.twist_msg.linear.x = linear_velocity
        self.twist_msg.angular.z = angular_velocity

    def shutdown(self):
        """ publish an empty twist message to stop the turtlebot"""
        rospy.loginfo("Stopping Turtlebot")
        self.cmd_vel.publish(Twist())
        rospy.sleep(1)
    
    # distance between the robot and the goal
    def goal_distance(self):
		return np.sqrt(((self.x - self.goal_x)**2)+((self.y - self.goal_y)**2))

    # determine if the robot is within the "acceptable goal distance"
    def reached_goal(self):
        return self.goal_distance() < ACCEPTABLE_GOAL_DIST

    #--from Devin's lab4
    # determine (wheel) velocities for an angle and return the rotation time
    def rotate(self, angle, omega = np.pi/6.0):
        time = angle / omega  ## omega is the rotational velocity
        speed = angle / np.abs(angle) * BASE_DIAMETER * omega / 2.0
        self.vel_from_wheels(-speed, speed, time)
        return time    

    # current yaw positon of the robot
    def yaw(self):
        quat = (
            self.odom.pose.pose.orientation.x,
            self.odom.pose.pose.orientation.y,
            self.odom.pose.pose.orientation.z,
            self.odom.pose.pose.orientation.w
        ) 
        euler = tf.transformations.euler_from_quaternion(quat)
        return euler[2]      
    
    # rotate accordingly to the surface normal of the local minimum distance
    def rotate_surf_norm(self, min_dist, min_angle, middle_val):
        
        # vector from the robot (center) to obstacle
        v1 = []
        v1.append(middle_val) 
        v1.append(0) 
        
        # local minimum vector coordinates
        loc_min_x = (min_dist * np.cos(min_angle)) 
        loc_min_y = (min_dist * np.sin(min_angle)) 
        
        # minimum distance vector (tangent line to local minimum)
        v2 = []
        v2x = loc_min_y
        v2y = -loc_min_x
        v2.append(v2x)
        v2.append(v2y)
        
		# compute the result (rotation angle)
        norm1 = LA.norm(v1)
        norm2 = LA.norm(v2)
        dot_prod = np.dot(v1, v2)         
        return np.arccos( dot_prod / (norm1 * norm2) )
        
    #--init-----------------------
    def __init__(self):
        
        rospy.init_node('bug2')
        rospy.on_shutdown(self.shutdown)
        
        # odometry messages
        self.odom = Odometry()
        self.starting_odom = Odometry()
        # bumper event message
        self.bumper_msg = BumperEvent()
        # twist message
        self.twist_msg = Twist()
        # laser scan
        #self.scan = LaserScan()   
        #self.laser_scan_msg = LaserScan()
        
        # robot's position 
        self.x = 0.0
        self.y = 0.0
        
        # robot's laserscan range data
        self.obs_ranges = []
        
        # SUBSCRIBERS AND PUBLISHERS
        # you might need to change topics depending on
        # whether you use simulation or real robots
        # subscribe to bumper events
        self.bumper_sub = rospy.Subscriber('/mobile_base/events/bumper', BumperEvent, self.bumper_callback,
                                           queue_size=1)
        # subscribe to odometry messages
        self.odom_sub = rospy.Subscriber('/odom', Odometry, self.odom_callback, queue_size=1)

        # subscribe to laser scanner
        self.laser_scan_sub = rospy.Subscriber('/scan', LaserScan, self.laser_scan_callback, queue_size=1)
        
        # publisher for twist messages
        self.cmd_vel = rospy.Publisher('cmd_vel_mux/input/navi', Twist, queue_size=10)
        
        # the goal position (2.5 meters)
        self.goal_x = 2.5
        self.goal_y = 0
        
        # travel at this speed when approaching an obstacle or the goal
        goal_seek_speed = 0.2   

        #loop rate
        r = rospy.Rate(50)
        
        self.bumper_pressed = False

        # set initial robot status info
        self.status = STATUS.GOALSEEK
        self.wf_status = WF_STATUS.NOT_SET
        
        # flags for status updates
        moving_to_obs = False
        moved_from_m_line = False
        moving_epsilon = False
        
        surf_ang = 0
        rot_time = 0
        init_rot_time = 0
        middle_val = 0

        # wait for laserscan data
        rospy.sleep(1)
        
        print("\n======RUNNING BUG(2) TANGENT========\n")
        
        while not rospy.is_shutdown():
			
            if self.bumper_pressed:
                print("Robot came in contact with (the) obstacle, FAILURE!")
                self.shutdown()
                break
			
			
            min_dist = 1000
            min_angle = 0 
            cur_ang_val = 0
            
            # extract necessary laser scan info 
            for i in range(0, len(self.obs_ranges)):
                val = self.obs_ranges[i]    
                if val < min_dist:
                    min_dist = val
                    ang_incr = i * KINECT_ANGLE_INC
                    if cur_ang_val < KINECT_ANGLE_MAX:
                        min_angle = KINECT_ANGLE_MAX - ang_incr
                    else: 
                        min_angle = KINECT_ANGLE_MIN + (ang_incr - KINECT_ANGLE_MAX)
            
            middle_val = self.obs_ranges[len(self.obs_ranges)/2] 
            
            # "re-allign" the robot if it is veering off the path (m-line)
            if self.status == STATUS.ALIGN:
				
                self.cmd_vel.publish(self.twist_msg)
                
                # move robot slightly (to place within veering threshold)
                if moving_epsilon:
					
                    if abs(self.y) < M_LINE_THRESHOLD:
                        self.twist_msg = Twist()
                        self.cmd_vel.publish(self.twist_msg)
                        self.status = STATUS.GOALSEEK
                        moving_epsilon = False
                # elde rotate the robot to keep it in acceptable m-line path         
                else:
                    cur_time = time.time()
                    if cur_time - init_time >= rot_time: 

                        self.twist_msg = Twist()
                        self.cmd_vel.publish(self.twist_msg)  
                        rot_time = 0
                        self.twist_msg.linear.x = goal_seek_speed
                        moving_epsilon = True
                        init_time = time.time()
                       
            elif self.status == STATUS.GOALSEEK:
                
                # if within (a realistic) distance to the goal, report SUCCESS (and quit)
                if self.reached_goal():
                    self.shutdown()
                    print("\nGoal reached, SUCCESS!")
                    print("\n======BUG(2) TANGENT COMPLETE========\n")   
                    break
                # if goal missed, report FAILURE (and quit)
                elif self.x > self.goal_x + ACCEPTABLE_GOAL_DIST:
                    self.shutdown()
                    print("\nGoal NOT reached, FAILURE!")
                    print("\n======BUG(2) TANGENT COMPLETE========\n")   
                    break   
                # if veering off the m-line path (as it will), re-align 
                elif abs(self.y) >= M_LINE_THRESHOLD:

                    self.twist_msg = Twist()
                    self.cmd_vel.publish(self.twist_msg)
                    
                    if self.yaw() < 0:
                        rot_time = self.rotate(np.deg2rad(20))
                    else:
                        rot_time = abs(self.rotate(np.deg2rad(-20)))
                        
                    self.status = STATUS.ALIGN
                    init_time = time.time()
                       
                # initial contact (W* boundary) with closest obstacle 
                elif min_dist <= COLLISION_AVOIDANCE_VAL:
                    print("Initial obsatcle hit point: Running wall follow algorithm")
                    surf_ang = self.rotate_surf_norm(min_dist, min_angle, middle_val)
                    self.status = STATUS.WALLFOLLOW
                    self.twist_msg = Twist()
                    self.cmd_vel.publish(self.twist_msg)
                    self.wf_status = WF_STATUS.rotating
                    init_time = time.time()
                else:
                    self.twist_msg.linear.x = goal_seek_speed
                    self.cmd_vel.publish(self.twist_msg)
            
            # upon m-line intersection, face the goal 
            elif self.status == STATUS.FACE_GOAL:

                self.cmd_vel.publish(self.twist_msg)
                
                # circumanvigated half the obstacle, now it's time to 
                # drive (straight) to the goal
                if abs(self.yaw()) <= SENSOR_TOLERANCE: 
                    self.twist_msg = Twist()
                    self.cmd_vel.publish(self.twist_msg)
                    self.status = STATUS.GOALSEEK
                
            # if following the obstacle walls (seeking m-line intersection)
            elif self.status == STATUS.WALLFOLLOW:
				
				# if reached the m-line, face the goal and make it home
                if moved_from_m_line and abs(self.y) <= SENSOR_TOLERANCE:
					
                    print("Reached the m line, time to go to the goal!")

                    self.twist_msg = Twist()
                    self.cmd_vel.publish(self.twist_msg)
                    
                    self.status = STATUS.FACE_GOAL
                    self.wf_status = WF_STATUS.NOT_SET
                    rot_time = abs(self.rotate(np.pi))
                    
                    init_time = time.time()
                    
                if self.wf_status == WF_STATUS.rotating:
                    if rot_time == 0:
                        rot_time = self.rotate(surf_ang) 
                    self.cmd_vel.publish(self.twist_msg)
                    
                    cur_time = time.time()
                    if cur_time - init_time >= rot_time:
                        self.wf_status = WF_STATUS.driving_straight
                        self.twist_msg = Twist()
                        self.cmd_vel.publish(self.twist_msg)
                        init_time = time.time()
                        rot_time = 0
                elif self.wf_status == WF_STATUS.driving_straight:
                    cur_time = time.time()
                    if cur_time - init_time <= 3:
                        self.twist_msg.linear.x = goal_seek_speed
                        self.cmd_vel.publish(self.twist_msg)
                        
                    else:
						
                        if not moved_from_m_line:
                            moved_from_m_line = True
                        
                        self.twist_msg = Twist()
                        self.cmd_vel.publish(self.twist_msg)
                       
                        if np.isnan(middle_val): 
                            surf_ang = -np.pi
                            self.wf_status = WF_STATUS.facing_obstacle
                        else: 												
                            surf_ang = self.rotate_surf_norm(min_dist, min_angle, middle_val)
                            self.wf_status = WF_STATUS.rotating
                        rot_time = self.rotate(surf_ang)
                        init_time = time.time()
                            
                elif self.wf_status == WF_STATUS.facing_obstacle:
					
                    if not moving_to_obs:
                        rot_time = abs(self.rotate(-np.pi))
                        cur_time = time.time()
                    
                    self.cmd_vel.publish(self.twist_msg)
                    
                    if not np.isnan(middle_val): 
                        
                        if min_dist > COLLISION_AVOIDANCE_VAL:
                            
                            if not moving_to_obs:
                                self.twist_msg = Twist()
                                self.cmd_vel.publish(self.twist_msg)
                                self.twist_msg.linear.x = goal_seek_speed
                                rot_time = 0
                            
                            moving_to_obs = True
                            
                        if moving_to_obs:
                            if min_dist <= COLLISION_AVOIDANCE_VAL: 
                                moving_to_obs = False
                            
                        if not moving_to_obs:
							
                            self.twist_msg = Twist()
                            self.cmd_vel.publish(self.twist_msg)
							
                            surf_ang = self.rotate_surf_norm(min_dist, min_angle, middle_val)
                            self.wf_status = WF_STATUS.rotating
                            rot_time = 0
                            init_time = time.time()
                            
            r.sleep()
         
        return 

if __name__ == '__main__':
    try:
        Bug2()
    except rospy.ROSInterruptException:
        pass
        
        
        